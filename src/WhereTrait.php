<?php
namespace Charm\Recordset;

trait WhereTrait {

    public function where(string $queryString, array $values=[]): RecordsetInterface {
        parse_str($queryString, $query);
        $result = $this;

        $offset = 0;
        $length = 100;

        foreach ($query as $name => $value) {
            if ($name === '$order') {
                if (!is_string($value)) {
                    continue;
                }
                if (strlen($value) === 0) {
                    continue;
                }
                $descending = $value[0] === '-';
                if ($descending) {
                    $value = substr($value, 1);
                }
                $result = $result->order($value, $descending);
            } else {
                if (!is_array($value)) {
                    $result = $result->eq($name, $value);
                } else {
                    foreach ($value as $op => $oVal) {
                        if ($oVal === '?') {
                            if (count($values) > 0) {
                                $oVal = array_shift($values);
                            } else {
                                $oVal = '';
                            }
                        }
                        $op = strtr($op, ['=' => 'eq', '<=' => 'lte', '>=' => 'gte', '<' => 'lt', '>' => 'gt', '~' => 'startsWith']);
                        switch($op) {
                            case 'eq':
                                $result = $result->eq($name, $oVal);
                                break;
                            case 'lt':
                                $result = $result->lt($name, $oVal);
                                break;
                            case 'lte':
                                $result = $result->lt($name, $oVal);
                                break;
                            case 'gt':
                                $result = $result->gt($name, $oVal);
                                break;
                            case 'gte':
                                $result = $result->gte($name, $oVal);
                                break;
                            case 'startsWith':
                                $result = $result->startsWith($name, $oVal);
                                break;
                        }
                    }
                }
            }
        }
        return $result;
    }
}
