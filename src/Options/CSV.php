<?php
namespace Charm\Recordset\Options;

class CSV extends Options {

    /**
     * Try to autodetect the CSV file format by reading a chunk of the file. If heuristics fail,
     * fall back to the defaults specified in the options. Heuristics work by reading the first
     * 64 KB of the file and counting separators, enclosures and escape symbols, and adds less
     * than 1 ms of parsing time.
     *
     * @readonly
     */
    public $useHeuristics; /* true if none of the 'lineLength', 'separator', 'enclosure' or 'escape' options are configured */

    /**
     * An array of column heading names, or null if the column columnNames should be fetched from
     * the first line of the CSV file.
     *
     * @readonly
     */
    public $columnNames = null;

    /**
     * If true, columns that can be accurately represented as a float or int will be converted to
     * that format.
     */
    public $convertNumbers = true;

    /**
     * If true, empty columns will be converted to null values
     */
    public $emptyIsNULL = true;

    /**
     * Speed up CSV parsing by providing a maximum line length for the CSV file.
     */
    public $lineLength = 0;

    /**
     * The separator parameter sets the field separator (one single-byte character only).
     *
     * @readonly
     */
    public $separator = ",";

    /**
     * The enclosure parameter sets the field enclosure character (one single-byte character only).
     *
     * @readonly
     */
    public $enclosure = '"';

    /**
     * The escape parameter sets the escape character (at most one single-byte character).
     * An empty string ("") disables the proprietary escape mechanism and uses character doubling
     * for escaping ("" instead of \").
     *
     * @readonly
     */
    public $escape = "\\";

    /**
     * @param array<string, mixed> $options
     */
    public function __construct(array $options=[]) {
        parent::__construct($options);
        $this->useHeuristics = $options['useHeuristics'] ?? (!isset($options['lineLength']) && !isset($options['separator']) && !isset($options['enclosure']) && !isset($options['escape']));
        $this->columnNames = !empty($options['columnNames']) ? ((array) $options['columnNames']) : $this->columnNames;
        $this->convertNumbers = $options['convertNumbers'] ?? $this->convertNumbers;
        $this->emptyIsNULL = $options['emptyIsNULL'] ?? $this->emptyIsNULL;
        $this->lineLength = (int) ($options['lineLength'] ?? $this->lineLength);
        $this->separator = (string) ($options['separator'] ?? $this->separator);
        $this->enclosure = (string) ($options['enclosure'] ?? $this->enclosure);
        $this->escape = (string) ($options['escape'] ?? $this->escape);
    }

}
