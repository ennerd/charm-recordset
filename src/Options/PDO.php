<?php
namespace Charm\Recordset\Options;

/**
 * Currently the PDORecordset does not have any custom options.
 * {@see Options} for more details.
 */
class PDO extends Options {

    public function __construct(array $options=[]) {
        parent::__construct($options);
    }

}
