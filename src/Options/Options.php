<?php
namespace Charm\Recordset\Options;

/**
 * Common configuration options for records returned by a recordset.
 */
class Options {

    /**
     * The class name for objects returned by the recordset. Instances will
     * be instantiated using the '__set_state' magic method without running
     * the constructor.
     *
     * If a 'factory' configuration option is provided, this class name is
     * ignored.
     *
     * @readonly
     * @var class-string
     */
    public $className = 'stdClass';

    /**
     * Mapping of database property names to object property names. If the
     * database column name is 'id', and the property name is '_id', you
     * would declare a property map like `['id' => '_id']`
     */
    public $propertyMapping = [];

    /**
     * A constructor function which returns object instances from rows
     * returned by the backend. The signature of the constructor function
     * is `function(array $properties): object`.
     *
     * @readonly
     * @var null|callable(array):object
     */
    public $factory; /* Options::createFactoryMethod($this->className) */

    /**
     * Set configuration options by passing an associative array of options
     * and values.
     *
     * @param array<string, mixed> $options
     */
    public function __construct(array $options=[]) {
        $this->className = $options['className'] ?? $this->className;
        $this->propertyMapping = $options['propertyMapping'] ?? $this->propertyMapping;
        $this->factory = $options['factory'] ?? $this->createFactoryMethod();
    }

    /**
     * Creates a function which will instantiate an object with properties
     * from an array. It supports private properties and undeclared properties.
     * If a property is undeclared and a __set() method exists, it will be
     * used.
     */
    private function createFactoryMethod() {
        $rc = new \ReflectionClass($this->className);
        $propertySetters = [];
        foreach ($rc->getProperties() as $prop) {
            if ($prop->isAccessible()) {
                $propertySetters[$prop->getName()] = \Closure::fromCallable([$prop, 'setValue']);
            } else {
                $propertySetters[$prop->getName()] = function($instance, $value) use ($prop) {
                    $prop->setAccessible(true);
                    $prop->setValue($instance, $value);
                    $prop->setAccessible(false);
                };
            }
        }
        return function(array $row) use ($rc, $propertySetters) {
            $instance = $rc->newInstanceWithoutConstructor();
            foreach ($row as $key => $value) {
                if (isset($this->propertyMapping[$key])) {
                    $key = $this->propertyMapping[$key];
                }
                if (isset($propertySetters[$key])) {
                    $propertySetters[$key]($instance, $value);
                } else {
                    $instance->$key = $value;
                }
            }
            if (method_exists($instance, '__wakeup')) {
                $instance->__wakeup();
            }
            return $instance;
        };
    }
}
