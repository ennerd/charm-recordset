<?php
namespace Charm\Recordset;

interface ExceptionInterface {

    const BACKEND_ERROR = 249;

    /**
     * When an operator is not recognized
     */
    const UNSUPPORTED_OPERATOR = 250;

    /**
     * When a requested length is > \Charm\Recordset::$length_max (default: 100).
     *
     * This causes an exception because different backends have different capabilities. A
     * backend powered by a remote API may need us to issue multiple requests to fetch
     * 100 rows. At the same time, a database backend has no problem fetching 100 rows
     * many times so the problem is easily worked around.
     */
    const LENGTH_TOO_HIGH = 251;

    /**
     * When a requested offset is < 0
     *
     * This causes an exception because it most likely is caused by a bug in the code
     * that uses this interface (such as a hard to find off-by-one error)
     */
    const OFFSET_TOO_LOW = 252;

    /**
     * When a requested offset is > \Charm\Recordset::getPageOffsetMax()
     *
     * Some backends don't support fetching rows beyond a certain limit.
     */
    const OFFSET_TOO_HIGH = 253;


    /**
     * When an expected file or resource was not found
     */
    const NOT_FOUND = 254;


}
