<?php
namespace Charm\Recordset;

interface RecordsetInterface {
    /**
     * Returns an iterable set of rows matching the specified filters and ordering
     */
    public function page(int $startOffset = 0, int $length=1000): iterable;

    /**
     * Returns a new instance where paged results are sorted
     */
    public function order(string $key, bool $descending=false): RecordsetInterface;

    /**
     * Returns a new instance where the value of property $key is
     * EQUAL to $value
     */
    public function eq(string $key, $value): RecordsetInterface;

    /**
     * Returns a new instance where the value of property $key is
     * LESS THEN $value
     */
    public function lt(string $key, $value): RecordsetInterface;

    /**
     * Returns a new instance where the value of property $key is
     * LESS THEN or EQUAL to $value
     */
    public function lte(string $key, $value): RecordsetInterface;

    /**
     * Returns a new instance where the value of property $key is
     * GREATER THEN $value
     */
    public function gt(string $key, $value): RecordsetInterface;

    /**
     * Returns a new instance where the value of property $key is
     * GREATER THEN or EQUAL to $value
     */
    public function gte(string $key, $value): RecordsetInterface;

    /**
     * Returns a new instance where the value of property $key is
     * STARTS with the string $value.
     */
    public function startsWith(string $key, string $value): RecordsetInterface;
}
