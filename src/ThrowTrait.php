<?php
namespace Charm\Recordset;

use Exception;
use LogicException;
use RuntimeException;

trait ThrowTrait {

    protected static function throwException(\Throwable $exception) {
        throw new class($exception->getMessage(), ExceptionInterface::BACKEND_ERROR, $exception) extends \Exception implements ExceptionInterface {};
    }

    protected static function throwIncorrectUsage(string $errorMessage) {
        throw static::logicException("Incorrect Usage: $errorMessage", 1);
    }

    protected static function throwBackendError(string $errorMessage) {
        throw static::runtimeException("Backend error: '$errorMessage'", ExceptionInterface::BACKEND_ERROR);
    }

    protected static function throwUnsupportedOperator(string $operator) {
        throw static::runtimeException("Unsupported operator '$operator'", ExceptionInterface::UNSUPPORTED_OPERATOR);
    }

    protected static function throwOffsetTooLow(string $message) {
        throw static::logicException("$message: Offset too low", ExceptionInterface::OFFSET_TOO_LOW);
    }

    protected static function throwOffsetTooHigh(string $message) {
        throw static::logicException("$message: Offset + high too high", ExceptionInterface::OFFSET_TOO_HIGH);
    }

    protected static function throwLengthTooHigh(string $message) {
        throw static::logicException("$message: Length too high", ExceptionInterface::LENGTH_TOO_HIGH);
    }

    protected static function throwNotFound(string $name) {
        throw static::runtimeException("Resource '$name' was not found", ExceptionInterface::NOT_FOUND);
    }

    private static function logicException(string $message, int $code) {
        return new class($message, $code) extends LogicException implements ExceptionInterface {};
    }

    private static function runtimeException(string $message, int $code) {
        return new class($message, $code) extends RuntimeException implements ExceptionInterface {};
    }
}
