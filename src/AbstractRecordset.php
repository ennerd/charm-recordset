<?php
namespace Charm\Recordset;

use EmptyIterator;
use Exception;
use Collator;
use JsonSerializable;
use Charm\Recordset;
use Charm\Recordset\Options\Options;

abstract class AbstractRecordset implements RecordsetInterface, \IteratorAggregate {
    use WhereTrait;
    use ThrowTrait;

    public static $salt = null;
    public static $hash_algo = 'sha256';
    public static $defaultCollator = null;

    protected $options;

    private $filters = [];
    private $orderBy = null;
    private $descending = false;

    protected function __construct(Options $options) {
        $this->options = $options;
    }

    /**
     * Implement fetching of rows from the backend. The only requirement is that the BackendRows object returned
     * will fetch all the rows from the backend.
     *
     * Performance can be greatly enhanced by respecting (sorting and start offset) and/or filters. For filters,
     * even partial filtering may benefit performance.
     *
     * Only implement row filtering if this is performed by an external database or uses deep knowledge of the
     * data structure to improve performance. Row by row based filtering is also performed by AbstractRecordset.
     *
     * @param $filters          Array of $filter[$keyName][$operatorName] => $value
     * @param $orderBy          A column that the backend *should* sort by, if possible
     * @param $descending       If the backend is able to sort, it should do so in reverse if possible
     * @param $offset           If the backend is able to sort, it should skip this many of the initial rows
     * @param $limit            For fully capable backends like a database, $limit can be used
     */
    abstract protected function fetchRows(array $filters, ?string $orderBy, bool $descending, int $offset, int $limit): BackendRows;

    /**
     * @TODO
     * Implement mechanisms to count the total number of matched rows. Should be possible to return an
     * estimate if not feasible to actually count the matches.
     *
     * Approaches:
     * - simulate the count by caching the results from the backend and counting the cached results.
     * - simulate an estimate by fetching a percentage of all existing rows and see how many rows are
     *   removed through filtering.
     * - let the backend provide the match count (if the backend supports all filters)
     */
    /* protected function totalCount(): int {
    }*/

    /**
     * @TODO
     * Implement a "fetchUniqueRow" method, for columns that are known to be unique
     */

    /**
     * @TODO
     * Implement a "getIndexedKey" for backends where one of the keys are known to be stored in sorted
     * order, for example in the case of a CSV file.
     */
    public final function page(int $pageOffset=0, int $pageLength=null): iterable {
        if ($pageOffset < 0) {
            static::throwOffsetTooLow(static::class.'::page($pageOffset, ...)');
        }
        if ($pageLength === null) {
            $pageLength = Recordset::getPageLengthMax($this);
        }
        $pageLength = max(0, $pageLength);
        if ($pageLength > Recordset::getPageLengthMax($this)) {
            static::throwLengthTooHigh(static::class.'::page(<offset>, $pageLength) (see '.Recordset::class.'::getPageLengthMax())');
        }
        if ($pageOffset + $pageLength > Recordset::getPageOffsetMax($this)) {
            static::throwOffsetTooHigh(static::class.'::page($pageOffset, $pageLength)');
        }
        if ($pageLength === 0) {
            // nothing to return
            return new EmptyIterator();
        }
        $filters = $this->getFilters();
        if ($filters === null) {
            // the filters ensure nothing will be returned from the backend
            return new EmptyIterator();
        }

        // this implementation returns a generator, so we put it in a separate method
        // to ensure validation happens even if nobody starts iterating the result set.
        return $this->processRows(
            $this->fetchRows($filters, $this->orderBy, $this->descending, $pageOffset, $pageLength),
            $filters,
            $pageOffset,
            $pageLength
        );
    }

    /**
     * May override this!
     *
     * The collator is used to compare values for sorting and comparisons if either or both of the values
     * are strings. NULL never matches a comparison with strings. TRUE is '1' and FALSE is '0'. Numbers are
     * converted to strings.
     */
    protected function getCollator(): \Collator {
        return static::getDefaultCollator();
    }

    /**
     * This function will process as many rows as needed from the source, filtering and sorting if needed.
     */
    private function processRows(BackendRows $rows, array $filters, int $pageOffset, int $pageLength): iterable {
        // number of rows that we have yielded
        $returnCount = 0;
        // number of rows that we removed from the result
        $discardCount = 0;
        // number of rows that have been buffered for sorting
        $bufferCount = 0;
        // number of rows that have been skipped to reach $pageOffset
        $skipCount = 0;

        // $wasSorted is true if the rows were sorted according to $this->orderBy
        // as they were received
        $direct = $this->orderBy === null || ($rows->getSortedColumn() === $this->orderBy && $rows->isDescending() === $this->descending);

        if ($direct) {
            $skipCount = $rows->getStartOffset();
        }

        // the backend is yielding rows in the order that we will yield rows
        foreach ($rows as $row) {
            // ignore this row if it fails the filter
            if (!static::isMatch($row)) {
                $discardCount++;
                continue;
            }
            if ($direct) {
                // skip the first $pageOffset rows
                if ($skipCount < $pageOffset) {
                    $skipCount++;
                    continue;
                }
                $returnCount++;
                yield $row;
                // stop if we have reached $pageLength
                if ($returnCount === $pageLength) {
                    break;
                }
            } else {
                $bufferCount++;
                $buffer[] = $row;
            }
        }

        if ($direct) {
            // the results were passed on directly to the receiver
            return;
        }

        /**
         * Apply sorting by using the collator
         */
        if ($this->orderBy !== null) {
            $orderBy = $this->orderBy;
            $collator = $this->getCollator();
            $sortKeys = [];
            foreach ($buffer as $row) {
                $val = $row->$orderBy;
                if (is_bool($val)) {
                    $sortKeys[] = $val ? 1 : 0;
                } elseif (is_int($val) || is_float($val)) {
                    $sortKeys[] = $val;
                } elseif (is_string($val)) {
                    $sortKeys[] = $collator->getSortKey($val);
                } else {
                    $sortKeys[] = $val;
                }
            }

            // to reverse the results
            $flipper = $this->descending ? -1 : 1;
            asort($sortKeys);
            if ($this->descending) {
                $sortKeys = array_reverse($sortKeys, true);
            }
/*
            uasort($sortKeys, function($a, $b) use ($collator, $flipper) {
                if (is_string($a) || is_string($b)) {
echo "USING STRING ".json_encode($a)." ".json_encode($b)."\n";
                    // sort using collator
                    if (!is_string($a)) {
                        $a = $collator->getSortKey((string) $a);
                    }
                    if (!is_string($b)) {
                        $b = $collator->getSortKey((string) $b);
                    }
                    if ($a > $b) {
                        return 1 * $flipper;
                    } elseif ($a === $b) {
                        return 0;
                    } else {
                        return -1 * $flipper;
                    }
                } else {
echo "USING MATH $a $b\n";
                    return ($a-$b) * $flipper;
                }
            });
*/
            /**
             * Apply paging information
             */
            $sortKeys = array_slice($sortKeys, $pageOffset, $pageLength, true);
            foreach ($sortKeys as $index => $sortKey) {
                yield $buffer[$index];
            }
            unset($buffer);
            return;
        } else {
            yield from array_slice($buffer, $pageOffset, $pageLength, false);
        }
    }

    final public function getIterator(): \Traversable {
        static::throwIncorrectUsage('Use '.static::class.'::page() to iterate');
    }

    /**
     * May override this!
     *
     * This will be the default collator for sorting and filtering all text columns for this
     * backend type.
     */
    protected static function getDefaultCollator(): \Collator {
        $collator = new \Collator(null);
        return $collator;
    }

    protected final function compare($value1, $value2) {
        if (!is_scalar($value1) || !is_scalar($value2)) {
            throw new class("Compare only legal with strings, numbers and booleans") extends \Exception implements ExceptionInterface {};
        }
        if ($value1 === null || $value2 === null) {
            throw new class("Compare with NULL will always return NULL") extends \Exception implements ExceptionInterface {};
        }

        if (is_bool($value1)) {
            $value1 = $value1 ? 1 : 0;
        }
        if (is_bool($value2)) {
            $value2 = $value2 ? 1 : 0;
        }
        if (!is_string($value1) && !is_string($value2)) {
            return $value2 - $value1;
        }
        return $this->getCollator()->compare((string) $value1, (string) $value2);
    }
    private $collatorCache = [];

    public final function isMatch(object $row): bool {
        foreach ($this->getFilters() as $key => $ops) {
            $rVal = $row->$key;

            if ($rVal === null && (!array_key_exists('eq', $ops) || $ops['eq'] !== null)) {
                // null can only match 'eq null'
                return false;
            }

            if (is_bool($rVal)) {
                $rVal = $rVal ? 1 : 0;
            }

            foreach ($ops as $op => $oVal) {
                if (is_bool($oVal)) {
                    $oVal = $oVal ? 1 : 0;
                }
                switch ($op) {
                    case 'eq':
                        if ($rVal !== $oVal && $this->compare($oVal, $rVal) !== 0) {
                            // only run $this->compare if this is not an exact match
                            return false;
                        }
                        break;
                    case 'lt':
                        if ($this->compare($oVal, $rVal) <= 0) {
                            return false;
                        }
                        break;
                    case 'lte':
                        if ($this->compare($oVal, $rVal) < 0) {
                            return false;
                        }
                        break;
                    case 'gt':
                        if ($this->compare($oVal, $rVal) >= 0) {
                            return false;
                        }
                        break;
                    case 'gte':
                        if ($this->compare($oVal, $rVal) > 0) {
                            return false;
                        }
                        break;
                    case 'startsWith':
                        if (substr((string) $rVal, 0, strlen((string) $oVal)) !== (string) $oVal) {
                            return false;
                        }
                        break;
                    default :
                        throw new class("Unknown operator '$op'") extends \RuntimeException implements ExceptionInterface {};
                        break;
                }
            }
        }
        return true;
    }

    /**
     * Returns an array of filters which should be applied to each
     * row of data. The function returns NULL if no rows could possibly
     * match the query.
     */
    private function getFilters(): ?array {
        $filters = [];
        foreach ($this->filters as $where) {
            list($key, $op, $value) = $where;

            if (array_key_exists($key, $filters) && array_key_exists($op, $filters[$key])) {
                if ($op === 'eq') {
                    if ($this->compare($filters[$key]['eq'], $value) !== 0) {
                        return null;
                    }
                    continue;
                }
                if ($op === 'gt' || $op === 'gte') {
                    // keep if higher
                    if ($this->compare($filters[$key][$op], $value) > 0) {
                        $filters[$key][$op] = $value;
                    }
                    continue;
                }
                if ($op === 'lt' || $op === 'lte') {
                    // use if lower
                    if ($this->compare($filters[$key][$op], $value) < 0) {
                        $filters[$key][$op] = $value;
                    }
                    continue;
                }
            } else {
                $filters[$key][$op] = $value;
            }
        }

        // detect conflicting operators
        foreach ($filters as $key => &$ops) {
            /**
             * Handle the combination of gte and lte. If identical, convert them
             * to an eq statement. In the next if block, the 'eq' statement will
             * cause any 'gt' or 'lt' statements to be removed.
             */
            if (array_key_exists('gte', $ops) && array_key_exists('lte', $ops) && $this->compare($ops['gte'], $ops['lte']) === 0) {
                // if we already have an 'eq' statement, its value must be equal
                if (array_key_exists('eq', $ops) && $this->compare($ops['eq'], $ops['lte']) !== 0) {
                    return null;
                }
                $ops['eq'] = $ops['gte'];
                unset($ops['gte'], $ops['lte']);
            }

            /**
             * If the 'eq' operator is used, we can remove any gtX and ltX
             * operators.
             */
            if (array_key_exists('eq', $ops)) {
                // eq is the simplest operator, so we'll remove the other operators after some checks
                if (array_key_exists('lt', $ops) && $this->compare($ops['eq'], $ops['lt']) >= 0) {
                    return null;
                }
                if (array_key_exists('lte', $ops) && $this->compare($ops['eq'], $ops['lte']) > 0) {
                    return null;
                }
                if (array_key_exists('gt', $ops) && $this->compare($ops['eq'], $ops['gt']) <= 0) {
                    return null;
                }
                if (array_key_exists('gte', $ops) && $this->compare($ops['eq'], $ops['gte']) < 0) {
                    return null;
                }

                unset($ops['lt'], $ops['lte'], $ops['gt'], $ops['gte']);
                // no more operators to process for this key
                continue;
            }

            /**
             * Remove either the 'gt' or the 'gte' operator if both are specified
             */
            if (array_key_exists('gt', $ops) && array_key_exists('gte', $ops)) {
                // remove either 'gt' or 'gte' whichever is stricter
                if ($this->compare($ops['gt'], $ops['gte']) >= 0) {
                    unset($ops['gte']);
                } else {
                    unset($ops['gt']);
                }
            }

            /**
             * Remove either the 'lt' or the 'lte' operator if both are specified
             */
            if (array_key_exists('lt', $ops) && array_key_exists('lte', $ops)) {
                // remove either 'lt' or 'lte' whichever is stricter
                if ($this->compare($ops['lt'], $ops['lte']) <= 0) {
                    unset($ops['lte']);
                } else {
                    unset($ops['lt']);
                }
            }

            /**
             * If 'lt' is combined with 'gt' or 'gte', then 'lt' must be larger.
             * If 'lte' is combined with 'gt' or 'gte, then 'lte' must be larger.
             * The special case where 'gte' is equal to 'lte' has already been
             * optimized into an 'eq' filter.
             */
            if (
                (array_key_exists('gt', $ops) || array_key_exists('gte', $ops)) &&
                (array_key_exists('lt', $ops) || array_key_exists('lte', $ops))
            ) {
                $gtX = array_key_exists('gt', $ops) ? $ops['gt'] : $ops['gte'];
                $ltX = array_key_exists('lt', $ops) ? $ops['lt'] : $ops['lte'];
                if ($this->compare($gtX, $ltX) >= 0) {
                    return null;
                }

            }
        }
        return $filters;
    }

    public final function order(string $key, bool $descending=false): AbstractRecordset {
        $rs = clone $this;
        $rs->orderBy = $key;
        $rs->descending = $descending;
        return $rs;
    }

    public final function eq(string $key, $value): AbstractRecordset {
        if ($value !== null && !is_scalar($value)) {
            throw new class("Invalid value for operator") extends Exception implements ExceptionInterface {};
        }
        $rs = clone $this;
        $rs->filters[] = [$key, 'eq', $value];
        return $rs;
    }

    public final function gt(string $key, $value): AbstractRecordset {
        if (!is_scalar($value)) {
            throw new class("Invalid value for operator") extends Exception implements ExceptionInterface {};
        }
        $rs = clone $this;
        $rs->filters[] = [$key, 'gt', $value];
        return $rs;
    }

    public final function gte(string $key, $value): AbstractRecordset {
        if (!is_scalar($value)) {
            throw new class("Invalid value for operator") extends Exception implements ExceptionInterface {};
        }
        $rs = clone $this;
        $rs->filters[] = [$key, 'gte', $value];
        return $rs;
    }

    public final function lt(string $key, $value): AbstractRecordset {
        if (!is_scalar($value)) {
            throw new class("Invalid value for operator") extends Exception implements ExceptionInterface {};
        }
        $rs = clone $this;
        $rs->filters[] = [$key, 'lt', $value];
        return $rs;
    }

    public final function lte(string $key, $value): AbstractRecordset {
        if (!is_scalar($value)) {
            throw new class("Invalid value for operator") extends Exception implements ExceptionInterface {};
        }
        $rs = clone $this;
        $rs->filters[] = [$key, 'lte', $value];
        return $rs;
    }

    public final function startsWith(string $key, $value): AbstractRecordset {
        if (!is_scalar($value)) {
            throw new class("Invalid value for operator") extends Exception implements ExceptionInterface {};
        }
        $rs = clone $this;
        $rs->filters[] = [$key, 'startsWith', $value];
        return $rs;
    }

    private static function hash(string $data): string {
        if (!is_string(self::$salt) || strlen(self::$salt) < 10) {
            throw new class(self::class.'::$salt is not configured with a salt of at least 10 characters') extends \Exception implements ExceptionInterface {};
        }

        return quoted_printable_encode(hash_hmac(self::$hash_algo, $data, self::$salt, true));
    }

    final public function jsonSerialize() {
        $str = static::class."\0".serialize($this);
        return $str."\0".self::hash($str);
    }
}

