<?php
namespace Charm\Recordset;

use Charm\Recordset\Options\Options;

/**
 * The simplest possible RecordSet implementation
 */
class ArrayRecordset extends GeneratorRecordset {
    public function __construct(array $rows, Options $options=null) {
        parent::__construct(function() use ($rows) {
            yield from $rows;
        }, $options ?? new Options());
    }
}
