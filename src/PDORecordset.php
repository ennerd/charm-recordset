<?php
namespace Charm\Recordset;

use Charm\Recordset;
use WeakReference;
use PDO;

class PDORecordset extends AbstractRecordset {

    use ThrowTrait;

    private $db, $query, $vars, $sqliteCollationFunction=null;

    public function __construct(PDO $db, string $query, array $vars=[], Options\PDO $options=null) {
        parent::__construct($options ?? new Options\PDO());
        $this->db = $db;
        $this->query = $query;
        $this->vars = $vars;
        if ($vars !== array_values($vars)) {
            static::throwIncorrectUsage("Don't use named params in your query");
        }
        if (method_exists($this->db, 'sqliteCreateFunction')) {
            $collator = $this->getCollator();
            $this->db->sqliteCreateFunction($this->getCollatorFunctionName(), function($value) use ($collator) {
                if (is_string($value)) {
                    return $collator->getSortKey($value);
                }
                return $value;
            });
            $this->sqliteCollationFunction = $this->getCollatorFunctionName();
        }
    }

    private function getCollatorFunctionName(): string {
        return str_replace("\\", "_", static::class.'_collator');
    }

    protected function fetchRows(array $filters, ?string $orderBy, bool $descending, int $offset, int $limit): BackendRows {
        $sql = 'WITH tbl AS ('.$this->query.') SELECT * FROM tbl';
        $vars = $this->vars;
        $wheres = [];
        foreach ($filters as $key => $ops) {
            foreach ($ops as $op => $value) {
                switch ($op) {

                    case 'gt':
                        $wheres[] = '"'.$key.'">?';
                        $vars[] = $value;
                        break;

                    case 'gte':
                        $wheres[] = '"'.$key.'">=?';
                        $vars[] = $value;
                        break;

                    case 'lt':
                        $wheres[] = '"'.$key.'"<?';
                        $vars[] = $value;
                        break;

                    case 'lte':
                        $wheres[] = '"'.$key.'">?';
                        $vars[] = $value;
                        break;

                    case 'startsWith':
                        $wheres[] = '"'.$key.'">=?';
                        $vars[] = $value;
                        $wheres[] = '"'.$key.'"<?';
                        $vars[] = $value;
                        break;

                    default :
                        static::throwUnsupportedOperator($op);
                }
            }
        }
        if (sizeof($wheres) > 0) {
            $sql .= ' WHERE '.implode(" AND ", $wheres);
        }

        if ($orderBy !== null) {
            if ($this->sqliteCollationFunction !== null) {
                $sql .= " ORDER BY ".$this->sqliteCollationFunction."($orderBy)";
            } else {
                $sql .= " ORDER BY $orderBy";
            }
            if ($descending) {
                $sql .= " DESC";
            }
        }

        return new BackendRows(function() use ($sql, $vars, $offset, $limit) {
            $state = [];
            try {
                $sql .= " LIMIT $offset,$limit";
                foreach ([PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION, PDO::ATTR_EMULATE_PREPARES => false, PDO::ATTR_STRINGIFY_FETCHES => false, PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => true] as $attr => $value){
                    try {
                        $state[$attr] = $this->db->getAttribute($attr);
                        $this->db->setAttribute($attr, $value);
                    } catch (\Throwable $e) {
                    }
                }
                $this->db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
                $stmt = $this->db->prepare($sql, [ PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY ]);
                if (!$stmt->execute($vars)) {
                    static::throwBackendError($stmt->errorInfo()[2]);
                }
                $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
                $stmt->closeCursor();

                // convert column types if needed
                if (isset($rows[0])) {
                    // if any of the columns are int/float, then we assume the backend provides the correct type
                    foreach ($rows[0] as $col => $val) {
                        if (is_int($val) || is_float($val)) {
                            goto no_convert_types;
                        }
                    }
                    $index = 0;
                    $hintMap = [
                        'int' => 'intval',
                        'long' => 'intval',
                        'num' => 'floatval',
                        'real' => 'floatval',
                        'double' => 'floatval',
                        'float' => 'floatval',
                    ];
                    $driverKey = null;
                    foreach ($rows[0] as $col => $val) {
                        $meta = $stmt->getColumnMeta($index++);
                        if ($driverKey === null) {
                            foreach ($meta as $driverKeyCandidate => $t) {
                                if (strpos($driverKeyCandidate, ':decl_type') !== false) {
                                    $driverKey = $driverKeyCandidate;
                                    break;
                                }
                            }
                        }
                        if ($driverKey === null) {
                            continue;
                        }
                        // translation for various backend types
                        $func = null;
                        if ($meta['precision'] !== 0) {
                            $func = 'floatval';
                        } else {
                            if (isset($meta[$driverKey])) {
                                foreach ($hintMap as $hint => $f) {
                                    if (stripos($meta[$driverKey], $hint) !== false) {
                                        $func = $f;
                                        break;
                                    }
                                }
                            }
                        }
                        if ($func !== null) {
                            foreach ($rows as &$row) {
                                $val = $func($row[$col]);
                                if ((string) $val === $row[$col]) {
                                    $row[$col] = $val;
                                }
                            }
                            unset($row);
                        }
                    }
                    no_convert_types:
                }
                foreach ($state as $attr => $value) {
                    $this->db->setAttribute($attr, $value);
                }
                foreach ($rows as $row) {
                    yield ($this->options->factory)($row);
                }
            } catch (ExceptionInterface $e) {
                foreach ($state as $attr => $value) {
                    $this->db->setAttribute($attr, $value);
                }
                throw $e;
            } catch (\Throwable $e) {
                foreach ($state as $attr => $value) {
                    $this->db->setAttribute($attr, $value);
                }
                if (strpos($e->getMessage(), "no such table")) {
                    static::throwNotFound($this->query);
                }
                static::throwException($e);
            }
        }, $offset, $orderBy, $descending);
    }

}
