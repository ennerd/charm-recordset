<?php
namespace Charm\Recordset;

/**
 * Carries a chunk of rows, with sorting information. The result can be partial, in which
 * case a string "resume token" for fetching the next chunk of rows should be provided.
 *
 * The sorting information will be used by the frontend to fetch as few rows as possible.
 * If no sorting information is provided, the backend will usually fetch the entire
 * data set.
 *
 * When rows marked as sorted, collation rules MUST be respected for string columns.
 */
final class BackendRows implements \Iterator {

    private $rowGenerator, $startOffset, $sortedCol, $descending;

    /**
     * Holds a collection of rows as matched by the backend (fully or partially filtered).
     *
     * @param $rowGenerator A callable which yields the matched rows. The generator should
     *                      try to optimize fetch performance by using techniques such as
     *                      bulk fetching and buffering and indexes.
     * @param $startOffset  The offset of the first row in the chunk. If the backend is unable
     *                      to skip rows, this value should be 0.
     * @param $rowCount     An estimate of the total number of rows remaining to fetch, if
     *                      this is possible to estimate.
     * @param $sortedCol   Name of the sorted column, if any.
     * @param $descending   True if the column is sorted in descending order
     */
    public function __construct(
        callable $rowGenerator,
        int $startOffset,
        ?string $sortedCol=null,
        bool $descending=null)
    {
        $this->rowGenerator = $rowGenerator();
        $this->rowGenerator->valid();
        $this->startOffset = $startOffset;
        $this->sortedCol = $sortedCol;
        $this->descending = !!$descending;
    }

    public function current() {
        return $this->rowGenerator->current();
    }

    public function key() {
        return $this->rowGenerator->key();
    }

    public function next() {
        return $this->rowGenerator->next();
    }

    public function rewind() {
        return $this->rowGenerator->rewind();
    }

    public function valid() {
        return $this->rowGenerator->valid();
    }

    public function getStartOffset(): int {
        return $this->startOffset;
    }

    public function getSortedColumn(): ?string {
        return $this->sortedCol;
    }

    public function isDescending(): bool {
        return $this->descending;
    }

    public function getResumeToken(): ?string {
        return $this->resumeToken;
    }
}
