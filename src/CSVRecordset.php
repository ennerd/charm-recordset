<?php
namespace Charm\Recordset;

/**
 * The generator will be iterated as few times as possible to
 * to provide the results requested and it will discard any
 * received rows outside of the page (offset and length)
 */
class CSVRecordset extends AbstractRecordset {

    use ThrowTrait;

    private $filename;

    public function __construct(string $filename, Options\CSV $options=null) {
        parent::__construct($options ?? new Options\CSV());
        $this->filename = $filename;
    }

    protected function fetchRows(array $filters, ?string $orderBy, bool $descending, int $pageOffset, int $pageLength): BackendRows {
        if (!file_exists($this->filename)) {
            self::throwNotFound($this->filename);
        }

        $fp = fopen($this->filename, 'r');

        $options = (object) ['lineLength' => $this->options->lineLength, 'separator' => $this->options->separator, 'enclosure' => $this->options->enclosure, 'escape' => $this->options->escape];
        if ($this->options->useHeuristics) {
            $chunk = stream_get_contents($fp, 65536, 0);
            $lines = explode("\n", $chunk);
            $separatorStats = [
                ';' => [],
                "\t" => [],
                ',' => [],
                '"' => [],
                "'" => [],
                '""' => [],
                "''" => [],
                "\\'" => [],
                '\"' => [],
            ];
            $options->lineLength = max(1024, array_reduce($lines, function(int $carry, string $line) use ($options, &$separatorStats) {
                foreach ($separatorStats as $separator => &$counts) {
                    $count = substr_count($line, $separator);
                    $counts[$count] = 1 + ($counts[$count] ?? 0);
                }
                return max($carry, strlen($line));
            }, 0) * 5);

            foreach ($separatorStats as $k => &$v) {
                unset($v[0]);
                if (count($v) === 0) {
                    unset($separatorStats[$k]);
                } else {
                    $v = array_sum($v);
                }
            }
            unset($v);
            asort($separatorStats);

            foreach ($separatorStats as $token => $count) {
                if (in_array($token, [';', "\t", ','])) {
                    $options->separator = $token;
                } elseif (in_array($token, ['"', "'"])) {
                    $options->enclosure = $token;
                } elseif (in_array($token, ['""', "''"])) {
                    $options->enclosure = '';
                } elseif (in_array($token, ["\\'", '\"'])) {
                    $options->enclosure = $token;
                }
            }
        }
        fseek($fp, 0);

        if ($this->options->columnNames === null) {
            $columnNames = fgetcsv($fp, $options->lineLength, $options->separator, $options->enclosure, $options->escape);
        } else {
            $columnNames = $this->options->columnNames;
        }

        $startOffset = ftell($fp);

        return new BackendRows(function() use ($fp, $columnNames, $options) {
            $rows = [];

            while (!feof($fp) && false !== ($row = fgetcsv($fp, $options->lineLength, $options->separator, $options->enclosure, $options->escape))) {
                foreach ($row as &$value) {
                    if ($value === '' && $this->options->emptyIsNULL) {
                        $value = null;
                    } elseif ($this->options->convertNumbers) {
                        if ($value == (int) $value) {
                            $value = (int) $value;
                        } elseif ($value == (float) $value) {
                            $value = (float) $value;
                        }
                    }
                }

                yield ($this->options->factory)(array_combine($columnNames, $row));
            }
        }, 0, null, null, null);
    }
}
