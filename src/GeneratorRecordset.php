<?php
namespace Charm\Recordset;

use Charm\Recordset\Options\Options;

/**
 * A very simple recordset type, which
 */
class GeneratorRecordset extends AbstractRecordset {

    use ThrowTrait;

    public function __construct(callable $source, Options $options=null) {
        parent::__construct($options ?? new Options());
        $this->source = $source;
    }

    protected function fetchRows(array $filters, ?string $orderBy, bool $descending, int $pageOffset, int $pageLength): BackendRows {
        return new BackendRows(function() use ($filters, $orderBy, $descending, $pageOffset, $pageLength) {
            try {
                foreach (($this->source)(['filters' => $filters, 'orderBy' => $orderBy, 'descending' => $descending, 'pageOffset' => $pageOffset, 'pageLength' => $pageLength]) as $row) {
                    yield (object) $row;
                }
            } catch (\Throwable $e) {
                if ($e instanceof ExceptionInterface) {
                    throw $e;
                }
                static::throwException($e);
            }
        }, 0, null, null, null);
    }

}
