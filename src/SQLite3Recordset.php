<?php
namespace Charm\Recordset;

use Charm\Recordset;
use WeakReference;

class SQLite3Recordset extends AbstractRecordset {

    use ThrowTrait;

    private $collatorFunction;
    private $db, $query, $vars, $rowFilter;
    private $bindIndex = 0;
    private static $collatorFunctions = [];

    public function __construct(\SQlite3 $db, string $query, array $vars=[], callable $rowFilter = null) {
        $connId = spl_object_id($db);
        if (!isset(self::$collatorFunctions[$connId])) {
            foreach (self::$collatorFunctions as $cId => $weakRef) {
                if (null === $weakRef->get()) {
                    unset(self::$collatorFunctions[$cId]);
                }
            }
            self::$collatorFunctions[$connId] = WeakReference::create($db);
            $collator = $this->getCollator();
            $db->createFunction($this->getCollatorFunctionName(), function($value) use ($collator) {
                if (is_string($value)) {
                    return $collator->getSortKey($value);
                }
                return $value;
            });
//            $db->createCollation($this->getCollatorFunctionName(), \Closure::fromCallable([$this, 'compare']));
        }
        $this->db = $db;
        $this->query = $query;
        $this->vars = $vars;
        $this->rowFilter = $rowFilter;
    }

    private function getCollatorFunctionName(): string {
        return str_replace("\\", "_", static::class.'_collator');
    }

    protected function fetchRows(array $filters, ?string $orderBy, bool $descending, int $offset, int $limit): BackendRows {
        $sql = 'WITH source_table AS ('.$this->query.') SELECT * FROM source_table';
        $vars = $this->vars;
        $wheres = [];
        foreach ($filters as $key => $ops) {
            foreach ($ops as $op => $value) {
                switch ($op) {

                    case 'gt':
                        $wheres[] = '"'.$key.'">:bound'.($this->bindIndex);
                        $vars[':bound'.($this->bindIndex++)] = $value;
                        break;

                    case 'gte':
                        $wheres[] = '"'.$key.'">=:bound'.($this->bindIndex);
                        $vars[':bound'.($this->bindIndex++)] = $value;
                        break;

                    case 'lt':
                        $wheres[] = '"'.$key.'"<:bound'.($this->bindIndex);
                        $vars[':bound'.($this->bindIndex++)] = $value;
                        break;

                    case 'lte':
                        $wheres[] = '"'.$key.'">:bound'.($this->bindIndex);
                        $vars[':bound'.($this->bindIndex++)] = $value;
                        break;

                    case 'startsWith':
                        $wheres[] = '"'.$key.'">=:bound'.($this->bindIndex);
                        $vars[':bound'.($this->bindIndex++)] = $value;
                        $wheres[] = '"'.$key.'"<:bound'.($this->bindIndex);
                        $vars[':bound'.($this->bindIndex++)] = $value."\xF48FBFBD";
                        break;

                    default :
                        static::throwUnsupportedOperator($op);
                }
            }
        }
        if (sizeof($wheres) > 0) {
            $sql .= ' WHERE '.implode(" AND ", $wheres);
        }

        if ($orderBy !== null) {
            $sql .= " ORDER BY ".$this->getCollatorFunctionName()."(\"$orderBy\")";
            if ($descending) {
                $sql .= " DESC";
            }
        }


        return new BackendRows(function() use ($sql, $vars, $offset, $limit) {
            $exceptionSetting = $this->db->enableExceptions(true);
            try {
                $sql .= " LIMIT $offset,$limit";
                $stmt = $this->db->prepare($sql);
                foreach ($vars as $k => $v) {
                    $stmt->bindParam($k, $v);
                }
//echo $stmt->getSQL()."\n";
                $res = $stmt->execute();
                $rows = [];
                while (false !== ($row = $res->fetchArray(SQLITE3_ASSOC))) {
                    $rows[] = (object) $row;
                }
                $res->finalize();
                $this->db->enableExceptions($exceptionSetting);
                yield from $rows;
            } catch (\Throwable $e) {
                if (strpos($e->getMessage(), 'no such table') !== false) {
                    static::throwNotFound($this->query);
                }
var_dump($sql, $e->getMessage());die();
                $this->db->enableExceptions($exceptionSetting);
                throw $e;
            }
        }, $offset, $orderBy, $descending);
    }

}
