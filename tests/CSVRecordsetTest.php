<?php

use Charm\Recordset\AbstractRecordset;
use Charm\Recordset\CSVRecordset;
use Charm\Recordset\ExceptionInterface;

/*
    test sorting
    test sorting with different collation
*/

/**
 * @covers CSVRecordset
 */
final class CSVRecordsetTest extends \RecordsetTests {

    private $rs;

    public function getRecordsets(): iterable {
        $rs = new CSVRecordset(__DIR__.'/files/countries.csv');
        $rawData = unserialize(file_get_contents(__DIR__.'/files/countries.phps'));
        foreach ($rawData as &$row) {
            $row = (array) $row;
        }
        yield 'countries.csv' => [ $rs, (object) ['strCol' => 'name', 'numCol' => 'latitude', 'serialized' => serialize($rawData)] ];
    }

    public function getColumns(): array {
        return ['country','latitude','longitude','name'];
    }

    /**
     * @covers ::page
     */
    public function test_csv_not_found() {
        // no exception is expected when constructing
        $rs = new CSVRecordset('file-not-found');

        $this->expectException(ExceptionInterface::class);
        $this->expectExceptionCode(ExceptionInterface::NOT_FOUND);
        // exception should happen when page is called for first time, even if we're not iterating
        $page = $rs->page(0, 1);
    }

    /**
     * @covers ::__construct
     * @covers ::page
     * @covers ::getBufferedRows
     * @covers AbstractRecordset::getFilters
     */
    public function test_csv_default_options() {
        $rs = new CSVRecordSet(__DIR__.'/files/countries.csv');
        $rows = [];
        foreach ($rs->page(0,10) as $row) {
            $rows[] = $row;
        }
        $countries = array_slice($this->getCountries(), 0, 10);
        $this->assertEquals($countries, $rows, "Returned rows does not match original");
    }

    /**
     * @covers ::page
     */
    public function test_csv_last_records() {
        $rs = $this->getCountriesRS();
        $rows = [];
        foreach ($rs->page(200, 100) as $row) {
            $rows[] = $row;
        }
        $countries = array_slice($this->getCountries(), 200, 100);
        $this->assertEquals($countries, $rows, "Returned rows does not match original");
    }

    protected function getCountries(): array {
        return unserialize(file_get_contents(__DIR__.'/files/countries.phps'));
    }

    protected function getCountriesRS() {
        return new CSVRecordSet(__DIR__.'/files/countries.csv');
    }


/*
    public function testFetchTooMuch() {
        $this->expectException(ExceptionInterface::class);
        foreach ($this->rs->page(0, 100000) as $row) {
        }
    }
*/
}
