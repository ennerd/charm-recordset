<?php

use PHPUnit\Framework\TestCase;
use Charm\Recordset\RecordsetInterface;
use Charm\Recordset\ExceptionInterface;
use Charm\Recordset;

abstract class RecordsetTests extends TestCase {

    /**
     * Recordsets must have all of these to be tested. Must yield [$recordset, (object) [ 'strCol' => $stringColumnName, 'numCol' => $numColName, 'serialized' => $rawData]
     *
     * $rawData must be an array of array values like [ [ 'id' => 123, 'name' => "Some Name" ], ... ].
     */
    abstract public function getRecordsets(): iterable;
    abstract public function getColumns(): array;


    public function getOrderingVariations(): iterable {
        foreach ($this->getRecordsets() as $name => $rsDef) {
            $rs = $rsDef[0];
            $info = $rsDef[1];

            foreach ($this->getColumns() as $col) {
                yield $name.' ORDER BY '.$col => [ $rs, $info, $col, false ];
                yield $name.' ORDER BY '.$col.' DESC' => [ $rs, $info, $col, true ];
            }
        }
    }

    /**
     * @dataProvider getOrderingVariations
     */
    public function test_ordering_variations($rs, $info, $column, $desc) {
        $previous = null;
//        echo "ORDER BY $column ".($desc ? 'DESC' : 'ASC')."\n";
        foreach ($rs->order($column, $desc)->page(0, 100) as $row) {
            if ($previous !== null) {
                $result = $this->compare($previous, $row->$column);
                if ($desc) {
                    $this->assertGreaterThanOrEqual(0, $result, json_encode($previous)." SHOULD COME AFTER ".json_encode($row->$column)." WHEN DESCENDING ORDER IS USED");
                } else {
                    $this->assertLessThanOrEqual(0, $result, json_encode($previous)." SHOULD COME AFTER ".json_encode($row->$column));
                }
            }
            $previous = $row->$column;
        }
    }

    /**
     * @dataProvider getRecordsets
     * @covers ::page
     * @covers ::getBufferedRows
     */
    public function test_check_provided_data($rs, $info) {
        $strCol = $info->strCol;
        $numCol = $info->numCol;
        $rows = [];

        for ($i = 0; $i < Recordset::getPageOffsetMax($rs); $i += 100) {
            foreach ($rs->page($i, Recordset::getPageLengthMax($rs)) as $row) {
                if ($row->$numCol !== null) {
                    $this->assertIsNumeric($row->$numCol, "Row::$numCol is not a numeric value (".json_encode($row).")");
                    $this->assertIsNotString($row->$numCol, "Row::$numCol is a numeric string, but must be int or float");
                }
                if ($row->$strCol !== null) {
                    $this->assertIsString($row->$strCol, "Row::$strCol is not a numeric value");
                }
                $rows[] = $row;
            }
        }
        $this->assertGreaterThanOrEqual(201, count($rows), "Test data must have at least 201 rows");
    }
    /**
     * @dataProvider getRecordsets
     * @covers ::page
     */
    public function test_page_exception_without_iterating($rs, $info) {
        $this->expectException(ExceptionInterface::class);
        $rs->page(-1, 10000000);
    }

    /**
     * @dataProvider getRecordsets
     * @covers ::page
     */
    public function test_page_offset_lt_0($rs, $info) {
        $this->expectException(ExceptionInterface::class);
        $this->expectExceptionCode(ExceptionInterface::OFFSET_TOO_LOW);
        $rs->page(-1, 1);
    }

    /**
     * @dataProvider getRecordsets
     * @covers ::page
     */
    public function test_page_offset_gt_maxoffset($rs, $info) {
        // boundary check
        $rs->page(Recordset::getPageOffsetMax($rs) - 1, 1);

        $this->expectException(ExceptionInterface::class);
        $this->expectExceptionCode(ExceptionInterface::OFFSET_TOO_HIGH);
        $rs->page(Recordset::getPageOffsetMax($rs), 1);
    }

    /**
     * @dataProvider getRecordsets
     * @covers ::page
     */
    public function test_page_length_0($rs, $info) {
        $this->assertEquals(0, count(iterator_to_array($rs->page(0,0))), "Fetch 0 rows failed");
    }

    /**
     * @dataProvider getRecordsets
     * @covers ::page
     */
    public function test_page_length_maxlength($rs, $info) {
        // boundary check
        $rs->page(0, Recordset::getPageLengthMax($rs));

        $this->expectException(ExceptionInterface::class);
        $this->expectExceptionCode(ExceptionInterface::LENGTH_TOO_HIGH);
        $rs->page(0, Recordset::getPageLengthMax($rs)+1);
    }

    /**
     * @dataProvider getRecordsets
     * @covers ::page
     */
    public function test_page_offset_and_length_gt_maxoffset($rs, $info) {
        // boundary check
        $rs->page(Recordset::getPageOffsetMax($rs)-10, 10);

        $this->expectException(ExceptionInterface::class);
        $this->expectExceptionCode(ExceptionInterface::OFFSET_TOO_HIGH);
        $rs->page(Recordset::getPageOffsetMax($rs)-10, 11);
    }

    /**
     * @dataProvider getRecordsets
     * @covers ::startsWith
     */
    public function test_startswith_percent($rs, $info) {
        $this->expectNotToPerformAssertions();
        $strCol = $info->strCol;
        $rows = [];
        foreach ($rs->startsWith($info->strCol, '%')->page(0, 10) as $row) {
            $rows[] = $row;
        }
        foreach ($rows as $row) {
            $this->assertEquals('%', substr($row->$strCol, 0, 1));
        }
    }

    /**
     * @dataProvider getRecordsets
     * @covers ::page
     */
    public function test_length_gt_maxlength($rs, $info) {
        // boundary check
        $rs->page(0, Recordset::getPageLengthMax($rs));

        $this->expectException(ExceptionInterface::class);
        $this->expectExceptionCode(ExceptionInterface::LENGTH_TOO_HIGH);
        $rs->page(0, Recordset::getPageLengthMax($rs)+1);
    }

    /**
     * @dataProvider getRecordsets
     * @covers ::page
     */
    public function test_no_exception_when_length_too_low($rs, $info) {
        $this->expectNotToPerformAssertions();
        $rs->page(0, -1);
    }


    protected function all_paging_tests($rs, $info) {
        $this->test_page_length_0($rs, $info);
        $this->test_page_ranges_correct($rs, $info);
    }

    /**
     * @dataProvider getRecordsets
     * @covers ::page
     * @covers ::getBufferedRows
     */
    public function test_page_ranges_correct($rs, $info) {
        // Recordset::page(0,8) and Recordset::page(9,12) should yield identical results as Recordset::page(1,20)
        $result = [];
        foreach ($rs->page(1,8) as $row) {
            $result[] = $row;
        }
        $this->assertEquals(8, count($result), "Expected 8 rows");
return;
        foreach ($rs->page(9, 12) as $row) {
            $result[] = $row;
        }
foreach ($result as $row) {
    echo json_encode($row)."\n";
}
        $this->assertEquals(20, count(iterator_to_array($rs->page(1, 20))), "Test data must contain at least 20 rows");
        $this->assertEquals($result, iterator_to_array($rs->page(1, 20), "Page 1-8 combined with page 9-20 does not match page 1-20 when fetched in one block"));
    }

    /**
     * @dataProvider getRecordsets
     * @covers ::order
     * @covers ::gt
     * @covers ::gte
     * @covers ::lt
     * @covers ::lte
     * @covers ::startsWith
     */
    public function test_immutable($rs, $info) {
        $variants = [$rs];
        $variants[] = $rs->order($info->numCol);
        $variants[] = $rs->order($info->numCol, true);
        $variants[] = $rs->order($info->numCol)->order($info->numCol, true);
        $variants[] = $rs->order($info->strCol);
        $variants[] = $rs->order($info->strCol, true);
        $variants[] = $rs->eq($info->numCol, 10);
        $variants[] = $rs->gt($info->numCol, 10);
        $variants[] = $rs->lt($info->numCol, 10);
        $variants[] = $rs->gte($info->numCol, 10);
        $variants[] = $rs->lte($info->numCol, 10);
        $variants[] = $rs->startsWith($info->numCol, 10);
        for ($i = 0; $i < count($variants); $i++) {
            for ($j = $i + 1; $j < count($variants); $j++) {
                $this->assertNotSame($variants[$i], $variants[$j]);
            }
        }

        $this->assertNotSame($rs->order($info->numCol),     $rs->order($info->numCol));
        $this->assertNotSame($rs->eq($info->numCol, 10),    $rs->eq($info->numCol, 10));
        $this->assertNotSame($rs->gt($info->numCol, 10),    $rs->gt($info->numCol, 10));
        $this->assertNotSame($rs->gte($info->numCol, 10),   $rs->gte($info->numCol, 10));
        $this->assertNotSame($rs->lt($info->numCol, 10),    $rs->lt($info->numCol, 10));
        $this->assertNotSame($rs->lte($info->numCol, 10),   $rs->lte($info->numCol, 10));
    }

    /**
     * Test paging with sorted numbers
     *
     * @dataProvider getRecordsets
     */
    public function test_paging_with_sorted_numbers($rs, $info) {
        $orig = unserialize($info->serialized);
        usort($orig, function($a, $b) use ($info) {
            $col = $info->numCol;
            $av = $a[$col];
            $bv = $b[$col];
            return $av - $bv;
        });
        $info->serialized = serialize($orig);
        $this->all_paging_tests($rs->order($info->numCol), $info);
    }

    /**
     * Test paging with sorted numbers descending
     *
     * @dataProvider getRecordsets
     */
    public function test_paging_with_sorted_numbers_desc($rs, $info) {
        $orig = unserialize($info->serialized);
        usort($orig, function($a, $b) use ($info) {
            $col = $info->numCol;
            $av = $a[$col];
            $bv = $b[$col];
            return $bv - $av;
        });
        $info->serialized = serialize($orig);
        $this->all_paging_tests($rs->order($info->numCol, true), $info);
    }

    /**
     * Test paging with sorted strings
     *
     * @dataProvider getRecordsets
     */
    public function test_paging_with_sorted_strings($rs, $info) {
        $collator = new \Collator(null);
        $orig = unserialize($info->serialized);
        $col = $info->strCol;
        usort($orig, function($a, $b) use ($col, $collator) {
            return $collator->compare($a[$col], $b[$col]);
        });
        $info->serialized = serialize($orig);

        $this->all_paging_tests($rs->order($info->strCol), $info);
    }


    /**
     * Test paging with sorted strings descending
     *
     * @dataProvider getRecordsets
     */
    public function test_paging_with_sorted_strings_desc($rs, $info) {
        $collator = new \Collator(null);
        $orig = unserialize($info->serialized);
        $col = $info->strCol;
        usort($orig, function($a, $b) use ($col, $collator) {
            return $collator->compare($a[$col], $b[$col]);
        });
        $info->serialized = serialize($orig);

        $this->all_paging_tests($rs->order($info->strCol, true), $info);
    }

    /**
     * test paging with sorted strings
     * test re-sorting
     * test sorting after filtering
     * test filtering after sorting
     * test filtering bools, strings, number on different column types
     */

/*

        // RecordsetInterface::count() should return the number of rows after filtering, but without paging
        $allRows = [];
        $actualCount = 0;
        $offset = 0;
        $previousRowCount = null;
        do {
            $rows = $rs->page($actualCount, 100);
            $rowCount = 0;
            foreach ($rows as $row) {
                $this->assertIsObject($row, "Returned rows must be PHP objects");
                $rowCount++;
                $allRows[] = 100;
            }
            if ($previousRowCount !== null) {
                $this->assertFalse($previousRowCount < 100 && $rowCount > 0, "Returned fewer than the requested number of rows, but more rows were available");
            }
            $actualCount += $rowCount;
            $previousRowCount = $rowCount;
        } while ($rowCount > 0);

        // Test count
        $row50 = $allRows[50];
        
    }

*/

    protected function compare($a, $b) {
        if (is_bool($a)) $a = $a ? 1 : 0;
        if (is_bool($b)) $b = $b ? 1 : 0;
        if (is_string($a) || is_string($b)) {
            if (!is_string($a)) $a = (string) $a;
            if (!is_string($b)) $b = (string) $b;
            $collator = new \Collator(null);
            return $collator->compare($a, $b);
        } else {
            return $a - $b;
        }
    }

}

