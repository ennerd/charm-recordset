<?php

use Charm\Recordset\AbstractRecordset;
use Charm\Recordset\ArrayRecordset;
use Charm\Recordset\ExceptionInterface;

/*
    test sorting
    test sorting with different collation
*/

/**
 * @covers ArrayRecordset
 */
final class ArrayRecordsetTest extends \RecordsetTests {

    private $rs;

    public function getRecordsets(): iterable {

        $rawData = unserialize(file_get_contents(__DIR__.'/files/countries.phps'));
        foreach ($rawData as &$row) {
            $row = (array) $row;
        }


        yield 'Generator(countries.csv)' => [
            new ArrayRecordset($rawData),
            (object) [
                'strCol' => 'name',
                'numCol' => 'latitude',
                'serialized' => serialize($rawData),
            ]
        ];
    }

    public function getColumns(): array {
        return ['country','latitude','longitude','name'];
    }

}
