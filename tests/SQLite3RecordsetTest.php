<?php

use PHPUnit\Framework\TestCase;
use Charm\Recordset\AbstractRecordset;
use Charm\Recordset\SQLite3Recordset;
use Charm\Recordset\ExceptionInterface;


/*
    test sorting
    test sorting with different collation
*/

/**
 * @covers CSVRecordset
 */
final class SQLite3RecordsetTest extends \RecordsetTests {

    public function getRecordsets(): iterator {
        ini_set('display_errors', '1');
        $db = new SQLite3(':memory:');
/*
        $collator = new \Collator(null);
        $db->createCollation('RS_COLLATION', function($a, $b) {

        });
*/
        $db->enableExceptions(true);
        $db->exec('CREATE TABLE countries (country TEXT,latitude REAL, longitude REAL, name TEXT)');
        $countries = unserialize(file_get_contents(__DIR__.'/files/countries.phps'));
        $insertCountry = $db->prepare('INSERT INTO countries VALUES (:country,:latitude,:longitude,:name)');
        foreach ($countries as $c) {
            $insertCountry->reset();
            foreach ($c as $i => $v) {
                $insertCountry->bindValue(is_int($i) ? 1+$i : $i, $v);
            }
            $res = $insertCountry->execute();
        }

        $rs = new SQLite3Recordset($db, 'SELECT * FROM countries', []);
        foreach ($countries as $i => $country) {
            $countries[$i] = (array) $country;
        }
        yield 'SELECT * FROM countries' => [ $rs, (object) ['strCol' => 'name', 'numCol' => 'latitude', 'serialized' => serialize($countries) ], ];

        $rs = new SQLite3Recordset($db, 'SELECT * FROM countries WHERE latitude > -15', []);
        foreach ($countries as $i => $country) {
            $countries[$i] = (array) $country;
        }
        $countriesGT0 = [];
        foreach($countries as $country) {
            if ($country['latitude'] > 0) {
                $countriesGT0[] = $country;
            }
        }
        yield 'SELECT * FROM countries WHERE latitude > -15' => [ $rs, (object) ['strCol' => 'name', 'numCol' => 'latitude', 'serialized' => serialize($countriesGT0) ] ];

        $rs = new SQLite3Recordset($db, 'SELECT * FROM countries ORDER BY latitude', []);
        foreach ($countries as $i => $country) {
            $countries[$i] = (array) $country;
        }
        yield 'SELECT * FROM countries ORDER BY latitude' => [ $rs, (object) ['strCol' => 'name', 'numCol' => 'latitude', 'serialized' => serialize($countries) ] ];


/*
        $db->loadExtension(dirname(__DIR__).'/build/csv/csv.so');
        $db->loadExtension('csv/csv.so');
        $db->exec("CREATE VIRTUAL TABLE temp.t1 USING csv(filename='".$db->escapeString(__DIR__.'/files/countries.csv')."')");
        $rawData = unserialize(file_get_contents(__DIR__.'/files/countries.phps'));
        foreach ($rawData as &$row) {
            $row = (array) $row;
        }

        $rs = new SQLite3Recordset($db, 'SELECT * FROM temp.t1', [], function($rows) {
            foreach ($rows as $i => &$row) {
                $row = (object) $row;
var_dump($row);
            }
            return $rows;
        });
*/
    }

    public function getColumns(): array {
        return ['country','latitude','longitude','name'];
    }

    public function getValidConstructorArgs(): iterable {
        // table exists should work
        $db = new SQLite3(':memory:');
        $db->exec('CREATE TABLE some_table (id INTEGER PRIMARY KEY, num FLOAT, str STRING)');
        yield [$db, 'SELECT * FROM some_table', []];

        // table don't exist should work
        yield [$db, 'SELECT * FROM some_table', []];

        // file based tables should be no different
        $tmpFile = tempnam(sys_get_temp_dir(), 'SQLite');
        $db = new SQLite3($tmpFile);
        $db->exec('CREATE TABLE some_table (id INTEGER PRIMARY KEY, num FLOAT, str STRING)');
        register_shutdown_function(function() use ($tmpFile) {
            unlink($tmpFile);
        });
        yield [$db, 'SELECT * FROM some_table', []];

        // file based tables should also work
        yield [$db, 'SELECT * FROM non_existing', []];
    }

    /**
     * @dataProvider getValidConstructorArgs
     */
    public function test_constructor_success($filename, $query, $vars) {
        $this->expectNotToPerformAssertions();
        $rs = new SQLite3Recordset($filename, $query, $vars);
    }

    public function test_nonexisting_table_query() {
        $this->expectException(ExceptionInterface::class);
        $this->expectExceptionCode(ExceptionInterface::NOT_FOUND);
        $rs = new SQLite3Recordset(new SQLite3(':memory:'), 'SELECT * FROM missing_table', []);

        // expect exception before iteration begins
        current($rs->page(0, 1));
    }

    public function test_existing_table_query() {
        $this->expectNotToPerformAssertions();
        $db = new SQLite3(':memory:');
        $res = $db->exec('CREATE TABLE existing_table (id INTEGER PRIMARY KEY, strcol VARCHAR(100), numcol FLOAT)');
        $rs = new SQLite3Recordset($db, 'SELECT * FROM existing_table', []);

        // expect no exception and no results
        foreach ($rs->page(0, 1) as $row) {
            $this->assertTrue(false);
        }
    }

}

