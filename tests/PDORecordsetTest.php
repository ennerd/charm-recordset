<?php

use PHPUnit\Framework\TestCase;
use Charm\Recordset\AbstractRecordset;
use Charm\Recordset\PDORecordset;
use Charm\Recordset\ExceptionInterface;


/*
    test sorting
    test sorting with different collation
*/

/**
 * @covers CSVRecordset
 */
final class PDORecordsetTest extends \RecordsetTests {

    public function getRecordsets(): iterator {
        ini_set('display_errors', '1');
        $db = new PDO('sqlite::memory:', null, null, [
            PDO::ATTR_EMULATE_PREPARES => false,
            PDO::ATTR_STRINGIFY_FETCHES => false,
        ]);
/*
        $collator = new \Collator(null);
        $db->createCollation('RS_COLLATION', function($a, $b) {

        });
*/
        $db->exec('CREATE TABLE countries (country VARCHAR(2),latitude FLOAT, longitude FLOAT, name VARCHAR(100))');
        $countries = unserialize(file_get_contents(__DIR__.'/files/countries.phps'));
        $insertCountry = $db->prepare('INSERT INTO countries VALUES (?,?,?,?)');
        foreach ($countries as $c) {
            $res = $insertCountry->execute(array_values((array) $c));
        }

        $rs = new PDORecordset($db, 'SELECT * FROM countries', []);
        foreach ($countries as $i => $country) {
            $countries[$i] = (array) $country;
        }
        yield 'SELECT * FROM countries' => [ $rs, (object) ['strCol' => 'name', 'numCol' => 'latitude', 'serialized' => serialize($countries) ], ];

        $rs = new PDORecordset($db, 'SELECT * FROM countries WHERE latitude > ?', [-15]);
        foreach ($countries as $i => $country) {
            $countries[$i] = (array) $country;
        }
        $countriesGT0 = [];
        foreach($countries as $country) {
            if ($country['latitude'] > 0) {
                $countriesGT0[] = $country;
            }
        }
        yield 'SELECT * FROM countries WHERE latitude > ?' => [ $rs, (object) ['strCol' => 'name', 'numCol' => 'latitude', 'serialized' => serialize($countriesGT0) ] ];

        $rs = new PDORecordset($db, 'SELECT * FROM countries ORDER BY latitude', []);
        foreach ($countries as $i => $country) {
            $countries[$i] = (array) $country;
        }
        yield 'SELECT * FROM countries ORDER BY latitude' => [ $rs, (object) ['strCol' => 'name', 'numCol' => 'latitude', 'serialized' => serialize($countries) ] ];


/*
        $db->loadExtension(dirname(__DIR__).'/build/csv/csv.so');
        $db->loadExtension('csv/csv.so');
        $db->exec("CREATE VIRTUAL TABLE temp.t1 USING csv(filename='".$db->escapeString(__DIR__.'/files/countries.csv')."')");
        $rawData = unserialize(file_get_contents(__DIR__.'/files/countries.phps'));
        foreach ($rawData as &$row) {
            $row = (array) $row;
        }

        $rs = new PDORecordset($db, 'SELECT * FROM temp.t1', [], function($rows) {
            foreach ($rows as $i => &$row) {
                $row = (object) $row;
var_dump($row);
            }
            return $rows;
        });
*/
    }

    public function getColumns(): array {
        return ['country','latitude','longitude','name'];
    }

    public function test_nonexisting_table_query() {
        $this->expectException(ExceptionInterface::class);
        $this->expectExceptionCode(ExceptionInterface::NOT_FOUND);
        $rs = new PDORecordset(new PDO('sqlite::memory:'), 'SELECT * FROM missing_table', []);

        // expect exception before iteration begins
        current($rs->page(0, 1));
    }

    public function test_existing_table_query() {
        $this->expectNotToPerformAssertions();
        $db = new PDO('sqlite::memory:');
        $res = $db->exec('CREATE TABLE existing_table (id INTEGER PRIMARY KEY, strcol VARCHAR(100), numcol FLOAT)');
        $rs = new PDORecordset($db, 'SELECT * FROM existing_table', []);

        // expect no exception and no results
        foreach ($rs->page(0, 1) as $row) {
            $this->assertTrue(false);
        }
    }

}

