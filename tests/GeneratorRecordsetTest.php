<?php

use Charm\Recordset\AbstractRecordset;
use Charm\Recordset\GeneratorRecordset;
use Charm\Recordset\ExceptionInterface;

/*
    test sorting
    test sorting with different collation
*/

/**
 * @covers GeneratorRecordset
 */
final class GeneratorRecordsetTest extends \RecordsetTests {

    private $rs;

    public function getRecordsets(): iterable {

        $rawData = unserialize(file_get_contents(__DIR__.'/files/countries.phps'));
        foreach ($rawData as &$row) {
            $row = (array) $row;
        }


        yield 'Generator(countries.csv)' => [
            new GeneratorRecordset(function() use ($rawData) {
                foreach ($rawData as $row) {
                    yield (object) $row;
                }
            }), 
            (object) [
                'strCol' => 'name',
                'numCol' => 'latitude',
                'serialized' => serialize($rawData),
            ]
        ];
    }

    public function getColumns(): array {
        return ['country','latitude','longitude','name'];
    }

}
