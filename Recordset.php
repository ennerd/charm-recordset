<?php
namespace Charm;

use Charm\Recordset\RecordsetInterface;

final class Recordset {
    /**
     * Recordset Length Limitation Settings
     *
     * PAGE_LENGTH_MAX 100      Restricts how many rows can be fetched per ($length < PAGE_LENGTH_MAX)
     *                          This options is intended to limit how many implicit backend calls are made,
     *                          if for example the backend is a remote API.
     *
     * PAGE_OFFSET_MAX 1000     Restricts how deep you can scan the index ($offset + $length < PAGE_OFFSET_MAX
     *                          This option exists because some cloud providers restrict how many rows can be
     *                          returned by an index scan, and we want to maintain transparency for the API
     *                          regardless of which backend it is powered by. We have many more reasons, some
     *                          of them have been explained below
     *
     * These options limit the size of recordsets fetched from backends. There are several reasons for this
     * design choice. The short explanation is that Google BigTable decided to restrict the max offset length
     * to 1000, arguing that if you need results longer than this - you probably need to reconsider your
     * application architecture.
     *
     * There are several arguments for restricting this value:
     *
     * - Costs                  Many backends are charged per request. Since this API hides the backend
     *                          details from the developers, they may not be aware that a simple 1000-page
     *                          request triggers 100 API requests for 10 rows - for example.
     * - Compatability          Backends that return few rows per request will require multiple
     *                          API calls to build a full result set and it may be difficult to emulate
     *                          very large result sets.
     * - Bad pattern            Having access to all the data in memory allows developers to implement
     *                          "cool" features which they could not have implemented in a large scale
     *                          application.
     * - Scalability            Large transfers cause noticeable network spikes and may block other requests.
     *                          Storing a lot of data in the application will reduce the available memory for
     *                          other processes.
     * - Consistency            It increases the likelyhood that a recordset is partially outdated
     *                          even before it has been received, especially when the data is fetched from
     *                          remote APIs or from multiple database shards.
     *
     * - Indexes loses value    Indexes make it fast to fetch a few rows, but if you are fetching almost
     *                          the entire table, it would be MUCH faster to read the entire file in sequence.
     */
    private const PAGE_LENGTH_MAX = 100;
    private const PAGE_OFFSET_MAX = 1000;

    /**
     * Returns how many rows can be returned by each call to RecordsetInterface::page()
     */
    public static function getPageLengthMax(RecordsetInterface $recordset): int {
        return self::getPageLengthMaxDefault();
    }

    /**
     * Returns the default value for how many rows can be returned by each call to RecordsetInterface::page()
     */
    public static function getPageLengthMaxDefault(): int {
        return self::PAGE_LENGTH_MAX;
    }

    /**
     * Returns the maximum offset we can return rows for (offset + length) in a call to RecordInterface::page()
     */
    public static function getPageOffsetMax(RecordsetInterface $recordset): int {
        return self::getPageOffsetMaxDefault();
    }

    /**
     * Returns the default value for the maximum offset we can return rows for (offset + length)
     */
    public static function getPageOffsetMaxDefault(): int {
        return self::PAGE_OFFSET_MAX;
    }
}
